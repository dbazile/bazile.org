---
date:    2023-06-14
subject: Comment on HN article titled 'The Reddit blackout has left Google barren and full of holes' (emphasis mine)
type:    quote
tags:
    - monopoly
    - allyourbase
---

**The problem is that the open internet is dying.** What used to be blogs and forums now is Reddit. Tens of thousands (hundreds of thousands?) of communities that had their own wiki, blog, forum, ... are now reduced to be a Subreddit.

The Internet has become fragile. One service goes down and everybody suffers. If the top 10 services went down most people would think that there is no Internet at all.

E-Mail is the last standing service that is way more open that the rest. But the raise of Whatsapp and equivalents are challenging that. One day all our communication will depend on a monopoly. We are starting to know what would have happened if AT&T have never been split.

<span class="quoth">@hourago, [link](https://news.ycombinator.com/item?id=36328554)</span>
