---
date:    2023-06-08
subject: Fixing Windows 10 spice-guest-tools Agent crash-on-start
tags:
    - libvirt
    - windows
    - virtualization
abstract: |
    I run my virtualization workloads in libvirt but recently had a problem with
    a new Windows 10 guest not being able to run vdagent (screen resolution
    resizing, clipboard sharing, sane mouse cursor, etc).
---

## Summary

### Problem

[spice-guest-tools' vdagent service](https://www.spice-space.org/spice-user-manual.html#agent)
won't start in Windows 10: it can be started from `services.msc` but it won't
stay running long after. This means no shared clipboard, no automatic screen
resolution resizing and the mouse and keyboard get forcefully captured on
desktop interaction.

(Host: Fedora 38, virt-manager 4.1.0, libvirt 9.0.0; Guest: Windows 10 20H2)


### Solution

Use BIOS instead of UEFI firmware for the VM (Virtual Machine Manager might get
this wrong when autodetecting from the .ISO during the install phase).

This might get fixed in libvirtd or spice-guest-tools eventually but as of
2023-06-08, comms on UEFI guests don't work and the agent Windows service will
crash on start.



## Details

It doesn't look like vdagent can open up comms with libvirt if the VM was
configured as a UEFI machine instead of BIOS. Now I didn't set the machine to
UEFI myself, mind you&mdash;Virtual Machine Manager does an autodetection of the
OS type on install and for some reason it thinks the Windows 10 .ISO is Windows
11, which defaults to using UEFI instead of BIOS:

![screenshot of incorrect autodetection](/public/writing/attachments/screenshot_vmm_autodetect_win11.png)

![screenshot of VMM firmware being set to UEFI instead of BIOS](/public/writing/attachments/screenshot_vmm_uefi.png)

I don't think you can just swap the configuration XML from UEFI to BIOS after
installing since the boot disk partition (at least for Windows) won't be
detected. I'm sure if you had the right tools you could convert the partition,
but I didn't and there's no way I'm paying for one so I had to do a full
reinstall to fix this.

As far as _why_ the spice protocol doesn't get thru on UEFI, maybe that has
something to do with secureboot or device signing or something, but that's
waaaay out of my depth to explain so I'll just leave it there.

### Finding the cause of the issue

This log message collected from the guest at `C:\Windows\temp\vdagent.txt` was
all I had to go on:

    4352::INFO::2023-06-01 11:35:52,649::run::***Agent started in session 1***
    4352::INFO::2023-06-01 11:35:52,649::log_version:0.9.0.0
    4352::INFO::2023-06-01 11:35:52,649::debug_print_config:\\.\DISPLAY1 [Before SetDisplayConfig] (0,0) (1024x768).
    4352::INFO::2023-06-01 11:35:52,649::set_display_config::path states says nothing changed
    4352::INFO::2023-06-01 11:35:52,649::consistent_displays::#qxls 1 #others 0
    4352::INFO::2023-06-01 11:35:52,649::init_vio_serial::Failed opening \\.\Global\com.redhat.spice.0, error 2

I only got to the solution I mentioned above because I had an old Windows VM
that _was_ working properly and used that as a reference to compare the
configurations.

### StackOverbearing

I originally posted this solution to the only place I found that came even close
to describing what I was seeing&mdash;[this StackOverflow community question](https://superuser.com/questions/1709681/virt-manager-spice-guest-tools-not-working-on-windows-10-guest/1786980)...
which was immediately nuked for reasons:

![screenshot - arbitrary displays of minor bureaucratic power are the best displays of power](/public/writing/attachments/stack_overbearing.png)

The original question, which has no accepted answer btw:

> <a href="https://superuser.com/questions/1709681/virt-manager-spice-guest-tools-not-working-on-windows-10-guest/1786980" style="color: inherit; font-weight: bold; font-size: 1.5em">virt-manager - spice-guest-tools not working on Windows 10 guest</a>
>
> ...
>
> I tried reinstalling spice-guest-tools, but that also does not fix the issue.
> What is going on here? Is it possible to get spice-guest-tools working with
> this setup, or should I just switch to VirtualBox or VMWare?

LOL ok bro lemme go ask a duplicate question and immediately answer it myself
then? `¯\_(ツ)_/¯`

So this post is here now. Hopefully this saves someone a few minutes/hours of
running around.


## Closing

As per usual, if you see any errors or oversights, please
[open an issue against the repo](https://gitlab.com/dbazile/bazile.org/issues/new?issue[title]=YOU%20ARE%20WRONG%20SIR).
