---
date:    2025-01-02
subject: Auto-activating window requesting attention in KDE Plasma
tags:
    - kde
    - linux
abstract: |
    I wrote a KWin plugin to auto-activate windows requesting attention instead
    of just flashing impotently in the taskbar (the KDE counterpart of GNOME's
    _SomeApplication is Ready_ problem that begat
    [noannoyance](https://extensions.gnome.org/extension/1236/noannoyance/)).
---

A month ago, I asked this question in KDE's forum:

> I’m new to KDE and I have some mismatched expectations with window activation
> and focus:
>
> 1. If I click on a Thunderbird notification for a new email, I expect TB to
>    bring itself to the foreground and show the email. Instead, clicking on the
>    notification does not bring up the TB window—all it does is add a very
>    discreet orange background to the taskbar icon with no other indication that
>    anything’s happened.
> 2. In Konsole, if I run /opt/sublime_text/sublime_text README.md to open a
>    file in Sublime Text, I expect ST to bring itself to the foreground and show
>    the file. If ST is not already open, it will do what I expect. If ST is
>    already open, it does not bring up the ST window—all it does is add a very
>    discreet orange background to the taskbar icon with no other indication that
>    anything’s happened.
>
> In both scenarios I want the app to raise itself to the foreground and take
> focus. Is there a way to allow certain apps to take focus when activated,
> either thru following a notification popup or from executing a shell command?

No response in over 30 days. Either people are on older versions of KDE that
don't have this problem or they're just fine with the current behavior. I
hope with every fiber of my being that it's not the latter. I wrote a KWin
plugin to "fix" this problem (Fedora 41, KDE Plasma 6.2.3):

<div class="codeblockname">~/.local/share/kwin/scripts/focuser/metadata.json</div>

```json
{
    "KPlugin": {
        "Name": "Window Activator/Focuser",
        "Description": "Auto-activate certain windows when they demand attention instead of just flashing silently in the taskbar",
        "Id": "focuser",
        "Version": "1.0",
        "License": "GPLv3",
        "Icon": "quickview",
        "Authors": [{"Name": "David Bazile"}]
    },
    "X-Plasma-API": "javascript",
    "X-Plasma-MainScript": "code/main.js",
    "KPackageStructure": "KWin/Script"
}
```

<div class="codeblockname">~/.local/share/kwin/scripts/focuser/contents/code/main.js</div>

```javascript
const WHITELIST = [
    'org.kde.konsole',
    'org.mozilla.firefox',
    'sublime_merge',
    'sublime_text',
    'tilix',
]


function main() {
    console.info('[focuser] INSTALLING')

    // Existing windows
    workspace.windowList().forEach(configure)

    // New windows
    workspace.windowAdded.connect(configure)

    console.info('[focuser] INSTALLED')
}

function configure(window) {
    if (!isAllowed(window)) {
        console.info('[focuser] SKIP DISALLOWED', window.resourceClass)
        return
    }

    console.info('[focuser] CONFIGURE', window.resourceClass)
    window.demandsAttentionChanged.connect(() => grantAttention(window))
}

function grantAttention(window) {
    console.info('[focuser] GRANT', window.resourceClass)
    workspace.activeWindow = window
}

function isAllowed(window) {
    return WHITELIST.includes(window.resourceClass)
}


main()
```
