---
date:    2025-02-19
subject: End of an Era (probably for real this time)
tags:
    - planet
    - freeloading
    - landsat-viewer
abstract: |
    Planet.com (again) seems to have stopped ~~freeloader~~ enthusiast
    use of their APIs. :(
---

[The last time this happened](/writing/2019/end_of_an_era.html),
access was restored a few days later mysteriously and
[landsat-viewer](https://landsat.dev.bazile.org/) lived to see another
day. But it looks like this time it's the search API and this time it
might **actually** be the end:

![401 on API search](/public/writing/attachments/screenshot_a_few_oofs.png)

I figured I'd log in directly and see if I just need to reissue my API
key, but of course I don't remember the credentials I used to sign up
8 years ago and "Reset Password" isn't yielding any emails which
makes me think they pruned their legacy users.

I looked at their new user signup and the only way to sign up outside
of being a megacorp with deep pockets is to be a college student:

![planet signup screenshot](/public/writing/attachments/screenshot_all_the_oofs.png)

With LV down, I might have to build more crapplications to host up
there but I am fresh out of ideas. 🤨
