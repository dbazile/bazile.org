import './libhljs.js'


export async function init() {
    console.debug('[init] scan for pre nodes')
    for (const e of document.querySelectorAll('pre')) {
        const language = e.querySelector('code')?.getAttribute('class')


        // Only highlight contexts that are explicitly marked
        if (language) {
            console.debug('[init] activate hljs for block', e)
            e.classList.add(language)
            hljs.highlightBlock(e)
        }
    }

    console.debug('[init] done')
}
