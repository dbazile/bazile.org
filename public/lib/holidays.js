const HOLIDAYS = [
    {
        name: 'Christmas',
        background: 'hsl(0, 57.5%, 52.9%)',
        icon: '🎄',
        test: d => d.getMonth() === 11 && d.getDate() >= 25 && d.getDate() <= 31,
    },
    {
        name: 'Easter',
        background: 'linear-gradient(to right, #ffb9d0, #ffe6b7)',
        icon: '<svg style="fill: hsl(0, 75%, 40%); width: 24px;" version="1.1" viewBox="0 0 100 100"><path d="m 83.7,72.4 v 2.1 H 62.5 V 72.4 Z M 72.0,62.9 h 2.1 v 21.1 h -2.1 z m -34.6,9.5 v 2.1 H 16.2 V 72.4 Z M 25.7,62.9 h 2.1 V 84.1 H 25.7 Z M 83.7,25.6 v 2.1 H 62.5 V 25.6 Z M 72.0,16.0 h 2.1 v 21.1 h -2.1 z m -34.6,9.5 v 2.1 H 16.2 V 25.6 Z M 25.7,16.0 h 2.1 V 37.2 H 25.7 Z M 100,37.4 V 62.5 H 91.6 V 37.4 Z m -8.1,8.3 v 8.3 H 8.1 V 45.8 Z M 8.3,37.4 V 62.5 H 0 V 37.4 Z M 37.4,0 H 62.5 V 8.3 H 37.4 Z m 8.3,8.1 h 8.3 V 91.8 H 45.8 Z M 37.4,91.6 H 62.5 V 100 H 37.4 Z" /></svg>',
        test: d => ['2024-03-31', '2025-04-20', '2026-04-05', '2027-03-28', '2028-04-16', '2029-04-01', '2030-04-21', '2031-04-13', '2032-03-28', '2033-04-17', '2034-04-09', '2035-03-25', '2036-04-13', '2037-04-05', '2038-04-25', '2039-04-10', '2040-04-01', '2041-04-21', '2042-04-06', '2043-03-29', '2044-04-17', '2045-04-09', '2046-03-25', '2047-04-14', '2048-04-05', '2049-04-18', '2050-04-10'].includes(simpledate(d)),
    },
    {
        name: 'Independence Day',
        background: 'linear-gradient(to right, royalblue, azure, tomato)',
        icon: '🇺🇸',
        test: d => simpledate(d).endsWith('07-04'),
    },
    {
        name: 'Marine Corps Birthday',
        background: 'crimson',
        icon: '<svg style="fill: black; width: 24px;" version="1.0" viewBox="0 0 13.92 16.85"><path d="M 5.13,0 5.86,0.73 V 1.46 L 5.13,2.19 4.39,1.46 0,3.66 H 5.13 L 5.83,5.93 C 5.18,6.04 4.58,6.29 4.06,6.66 L 5.97,6.53 6.25,7.43 7.52,6.73 8.06,7.33 7.33,8.79 5.13,9.52 6.59,10.26 7.86,9.75 9.36,10.36 8.55,12.30 6.59,13.92 5.86,11.07 2.93,9.28 V 7.83 C 2.46,8.52 2.19,9.36 2.19,10.26 c 0,1.02 0.35,1.97 0.94,2.72 l -0.94,0.94 c 0,0 -1.46,-2.20 -1.46,-2.93 C 0.73,10.26 0,10.99 0,11.72 c 0,0.57 0.52,2.13 1.07,3.12 0.09,0.17 -0.03,0.92 -0.03,0.92 0,0 0.84,-0.10 0.96,-0.03 0.98,0.57 2.54,1.11 3.12,1.11 0.73,0 1.46,-0.73 0.73,-0.73 -0.73,0 -2.93,-1.46 -2.93,-1.46 L 3.87,13.71 c 0.74,0.59 1.69,0.94 2.72,0.94 2.42,0 4.39,-1.96 4.39,-4.39 0,-1.02 -0.35,-1.97 -0.94,-2.72 L 10.99,6.59 11.72,7.33 12.46,6.59 10.26,4.39 9.52,5.13 10.26,5.86 9.31,6.80 C 8.75,6.36 8.08,6.05 7.35,5.92 L 8.06,3.66 h 3.13 c -0.12,0.21 -0.19,0.46 -0.19,0.73 0,0.80 0.65,1.46 1.46,1.46 0.80,0 1.46,-0.65 1.46,-1.46 0,-0.80 -0.65,-1.46 -1.46,-1.46 -0.18,0 -0.36,0.03 -0.53,0.10 L 8.79,1.46 8.06,2.19 7.33,1.46 6.59,0 Z m 7.33,3.58 c 0.44,0 0.80,0.36 0.80,0.80 0,0.44 -0.36,0.80 -0.80,0.80 -0.44,0 -0.80,-0.35 -0.80,-0.80 0,-0.44 0.35,-0.80 0.80,-0.80 z"/></svg>',
        test: d => simpledate(d).endsWith('11-10'),
    },
    {
        name: 'Matter of Importance',
        background: 'hsl(348.7, 100%, 83.3%)',
        icon: '🌸',
        test: d => simpledate(d).endsWith('03-26'),
    },
    {
        name: 'Memorial Day',
        background: 'hsl(98, 50%, 35%)',
        icon: '<svg style="width: 36px;" version="1.1" viewBox="0 0 845.1 857.5"><path style="fill: tan;" d="m 0,844.2 c 0,0 16.2,-8.8 43.6,-21.4 10.4,-4.7 22.4,-10.0 35.8,-15.6 19.9,-8.2 42.8,-17.1 67.7,-25.4 71.8,-24.1 160.0,-44.6 241.3,-37.9 289.5,23.7 456.5,84.5 456.5,84.5 V 857.5 H 0 Z"/><path style="fill: black;" d="m 598.5,78.8 -106.6,-15.4 -31.0,194.1 -53.0,28.4 -9.1,110.9 31.2,15.7 -1.4,13.3 -7.6,5.9 -36.2,249.9 -43.5,35.5 50.3,13.4 -19.0,89.6 13.4,0.2 29.9,-133.3 10.6,-4.4 56.1,-238.3 -12.4,-10.9 4.7,-22.8 95.0,49.7 26.8,-56.8 -96.4,-48.1 6.2,-34.9 92.3,-26.7 -10.2,-42.8 -77.2,27.7 -4.1,-25.0 z m -154.7,214.8 -17.0,108.3 -19.9,-9.6 14.0,-90.4 z m -52.1,393.1 -5.6,33.7 -28.7,-6.9 z"/><path style="fill: olive;" d="m 614.8,254.8 c -10.8,9.7 -9.5,16.7 -9.5,16.7 0,0 -44.6,-63.3 -75.5,-97.2 -30.8,-33.9 -96.3,-91.9 -96.3,-91.9 0,0 9.3,1.3 18.9,-7.3 49.1,-44.4 117.7,-58.0 175.5,4.6 57.8,62.7 36.1,130.6 -13.0,175.1 z"/></svg>',
        test: d => simpledate(d) === simpledate(nthday(d.getFullYear(), 5, 'mon', -1)),
    },
    {
        name: 'Saint Patrickʼs Day',
        background: 'hsl(98, 50%, 50%)',
        icon: '🍀',
        test: d => simpledate(d).endsWith('03-17'),
    },
    {
        name: 'Saint Valentineʼs Day',
        background: 'hsl(0, 100%, 62.2%)',
        icon: '❤️',
        test: d => simpledate(d).endsWith('02-14'),
    },
    {
        name: 'Thanksgiving',
        background: 'tan',
        icon: '🦃',
        test: d => simpledate(d) === simpledate(nthday(d.getFullYear(), 11, 'thu', 4)),
    },
    {
        name: 'Veterans Day',
        background: 'olivedrab',
        icon: '🪖',
        test: d => simpledate(d).endsWith('11-11'),
    },
]


export async function init() {
    const stylesheet = document.createElement('style')
    stylesheet.type = 'text/css'
    stylesheet.textContent = `
        .holiday-bar {
            height: 3px;
            font-size: 30px;
        }

        .holiday-bar-icon {
            position: absolute;
            top: 15px;
            right: 15px;
        }
    `
    document.head.appendChild(stylesheet)

    const now = new Date()
    for (let holiday of HOLIDAYS) {
        if (holiday.test(now)) {
            setholiday(holiday, now)
            break
        }
    }

    // ¯\_(ツ)_/¯
    window.nthday = nthday
    window.setholiday = setholiday
}


//
// Internals
//

function nthday(y, m, day, nth) {
    const DAYS = 'SUN MON TUE WED THU FRI SAT'.split(' ')

    if (typeof day === 'string') {
        day = DAYS.indexOf(day.toUpperCase().trim())
    }
    if (typeof day !== 'number' || day < 0 || day > 6) {
        throw new Error(`this is not a day buddy: ${day}`)
    }

    console.groupCollapsed(
        '%c nthday ',
        'background-color: #ddd;',
        `looking for ${nth}${{1: 'st', 2: 'nd', 3: 'rd'}[nth] || 'th'} ${DAYS[day]} in ${m}/${y}`)

    m -= 1

    // Create working date
    const d = new Date(`${y}-01-01`)
    d.setMinutes(d.getTimezoneOffset())
    d.setMonth(m)

    // Determine scan direction
    let increment = 1
    if (nth < 0) {
        console.debug('reverse direction')
        increment = -1
        nth = Math.abs(nth)
        d.setMonth(d.getMonth() + 1)
        d.setDate(0)
    }

    // Scan
    let timesSeen = 0
    let result = null
    while (d.getMonth() === m) {
        console.debug('test:', d)

        if (d.getDay() === day) {
            timesSeen += 1
        }

        if (timesSeen === nth) {
            console.debug('found it:', d)
            result = d
            break
        }

        d.setDate(d.getDate() + increment)
    }

    if (!result) {
        console.debug('no result')
    }

    console.groupEnd()

    return result
}


function setholiday(holiday, d = new Date()) {
    console.debug(`[setholiday] "${holiday?.name || 'none'}"`)

    Array.from(document.querySelectorAll('.holiday-bar'))
        .forEach(e => e.parentNode.removeChild(e))

    if (typeof holiday === 'number') {
        holiday = HOLIDAYS[holiday]
    }
    if (!holiday?.name) {
        return
    }

    const element = document.createElement('div')
    element.className = 'holiday-bar'
    element.title = `${holiday.name} (${simpledate(d).substring(5).replace('-', '/')})`
    element.style.background = holiday.background

    if (holiday.size) {
        element.style.height = `${holiday.size}px`
    }

    const icon = document.createElement('span')
    icon.className = 'holiday-bar-icon'
    icon.innerHTML = holiday.icon
    element.appendChild(icon)

    document.body.insertBefore(element, document.body.firstChild)
}


function simpledate(d) {
    const d2 = new Date(d)
    d2.setMinutes(d2.getMinutes() - d2.getTimezoneOffset())
    return d2.toISOString().substring(0, 10)
}
