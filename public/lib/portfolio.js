import * as transitioner from './transitioner.js'


export async function init() {
    console.debug('[init] subscribe to hashchange events')

    window.addEventListener('hashchange', onHashChange)
    transitioner.beforeNext(teardown)
    onHashChange()

    // This is needed because the :target CSS pseudoselector apparently doesn't
    // recompute if entering a hashed URL from a history.pushState'd URL.  This
    // manifests itself when moving _away_ from portfolio to another route, then
    // going back in history.
    //
    // Now all I need to figure out is how to avoid the stinkin' 15px "sink" when
    // bouncing the hash... >_<
    //
    // Refer to https://bugs.webkit.org/show_bug.cgi?id=83490
    if (location.hash) {
        console.debug('[init] bounce hash')
        location.replace(location.href)
    }
}


function onHashChange() {
    const tiles = document.querySelector('.tiles')
    if (location.hash) {
        tiles.classList.add('tiles--hasActiveTile')
    }
    else {
        tiles.classList.remove('tiles--hasActiveTile')
    }

    Array.from(document.querySelectorAll('.tiles__tile'))
        .forEach(element => {
            if (element.querySelector('a').hash === location.hash) {
                element.classList.add('tiles__tile--isActive')
            }
            else {
                element.classList.remove('tiles__tile--isActive')
            }
        })
}


function teardown() {
    console.debug('[teardown] unsubscribe from hash change events')
    window.removeEventListener('hashchange', onHashChange)
}
