// main.js

import * as iframer from './iframer.js'
import * as transitioner from './transitioner.js'
import * as holidays from './holidays.js'


if (location.port === '8000') {
    const es = new EventSource('/sse')
    es.onmessage = (m) => m.data === 'RELOAD' ? location.reload() : console.info('[sse] recv:', m.data)
    window.addEventListener('beforeunload', () => es.close())
}


await Promise.all([
    holidays.init(),
    iframer.init(),
    transitioner.init(),
])
