export function init() {
    document.addEventListener('DOMContentLoaded', () => {
        console.debug('[init] looking for deferrable iframes')

        Array.from(document.querySelectorAll('iframe[data-defer]'))
            .forEach(iframe => {
                const callback = debounce(20, () => {
                    if (!isNearViewport(iframe)) {
                        return
                    }

                    iframe.src = iframe.dataset.defer

                    window.removeEventListener('scroll', callback)
                })

                console.debug('[init] found deferrable iframe:', iframe)

                window.addEventListener('scroll', callback)

                callback()
            })

        console.debug('[init] done')
    })
}


//
// Internals
//

function debounce(delay, callback) {
    let handle
    return (...args) => {
        clearTimeout(handle)
        handle = setTimeout(() => callback(...args, delay))
    }
}


function isNearViewport(element) {
    const pos = element.getBoundingClientRect()
    const distanceY = pos.top - document.documentElement.clientHeight
    const minY = document.documentElement.clientHeight + (pos.height * 2)

    return distanceY >= 0
        ? distanceY < minY
        : distanceY > (minY * -1)
}
