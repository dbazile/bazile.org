const CLASS_STATE_INITIAL = 'state--initial'
const CLASS_STATE_ERROR = 'state--error'
const CLASS_STATE_HAS_RESULTS = 'state--hasResults'
const CLASS_STATE_NO_RESULTS = 'state--noResults'
const CLASS_STATE_SEARCHING = 'state--searching'
const CLASS_TAG_MATCH = 'tag--matches'

let _previousAc
let _templateForBlog
let _templateForPortfolio


async function main() {
    if (location.port === '8000') {
        const es = new EventSource('/sse')
        es.onmessage = (m) => m.data === 'RELOAD' ? location.reload() : console.info('[sse] recv:', m.data)
        window.addEventListener('beforeunload', () => es.close())
    }

    // Collect templates
    _templateForBlog = document.querySelector('.result--blog')
    _templateForBlog.parentNode.removeChild(_templateForBlog)
    _templateForPortfolio = document.querySelector('.result--portfolio')
    _templateForPortfolio.parentNode.removeChild(_templateForPortfolio)

    // Execute first query if present
    const query = readQuery()
    if (query) {
        updateQueryDOM(query)
        search(query)
    }

    // Attach event listeners
    window.addEventListener('popstate', onPopState)
    document.querySelector('form').addEventListener('submit', onSubmit)
    Array.from(document.querySelectorAll('.placeholder__dismissButton'))
        .forEach(button => button.addEventListener('click', onDismiss))

    // ¯\_(ツ)_/¯
    window.search = search
}


function appendResult(result) {
    const node = _templateForBlog.cloneNode(true)
    node.querySelector('.result__subject').textContent = result.subject
    node.querySelector('.result__hyperlink').href = result.uri.replace(/^https?:\/\/[^/]+/g, '')
    node.querySelector('.result__description').textContent = result.description

    const tags = node.querySelector('.result__tags')
    const query = readQuery()

    result.tags.map(s => {
        const tag = document.createElement('li')
        tag.textContent = s
        if (query === 'tagged:' + s) {
            tag.className = CLASS_TAG_MATCH
        }
        tags.appendChild(tag)
    })

    document.querySelector('.results').appendChild(node)
}


function clearResults() {
    const results = document.querySelector('.results')
    while (results.firstChild) {
        results.removeChild(results.firstChild)
    }
}


function onDismiss() {
    clearResults()
    document.documentElement.className = CLASS_STATE_INITIAL
}


function onSubmit(event) {
    event.preventDefault()
    const query = document.querySelector('input').value
    writeQuery(query)
    search(query)
}


function onPopState() {
    const query = readQuery()
    updateQueryDOM(query)
    search(query)
}


function readQuery() {
    return decodeURIComponent(location.search.substr(1))
        .replace(/^tagged=/, 'tagged:')
        .replace(/^keyword=/, '')
}


async function search(query) {
    const CANCELED = 'CANCELED'

    console.debug('%c search for `%s` %c %s', 'background-color: #ddd', query)

    // Configure request abort
    if (_previousAc) {
        _previousAc.abort(CANCELED)
    }
    const ac = _previousAc = new AbortController()

    // Execute search
    try {
        document.documentElement.className = CLASS_STATE_SEARCHING

        const res = await fetch(`${document.querySelector('form').action}?query=${encodeURIComponent(query)}`, { signal: ac.signal })
        if (res.status !== 200) {
            throw Error(`API returned HTTP${res.status}`)
        }

        clearResults()

        const response = await res.json()
        document.querySelector('.metrics__count').textContent = response.results.length
        document.querySelector('.metrics__query').textContent = query
        response.results.forEach(appendResult)
        document.documentElement.className = response.results.length ? CLASS_STATE_HAS_RESULTS : CLASS_STATE_NO_RESULTS

        _previousAc = null
    }
    catch (e) {
        if (e === CANCELED) {
            return
        }

        console.error('search failed:', e)
        document.documentElement.className = CLASS_STATE_ERROR
    }
}


function updateQueryDOM(query) {
    document.querySelector('input').value = query
}


function writeQuery(query) {
    let href = location.href.replace(/\?.*$/, '')
    if (query.match(/^tagged:/)) {
        href += '?tagged=' + encodeURIComponent(query.replace('tagged:', ''))
    }
    else {
        href += '?keyword=' + encodeURIComponent(query)
    }
    history.pushState(null, null, href)
}

//
// Bootstrapping
//

main()
