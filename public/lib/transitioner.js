const NS_XLINK             = 'http://www.w3.org/1999/xlink'
const PLACEHOLDER_DURATION = location.port === '8000' ? 0 : 2000
const SAME_ORIGIN          = location.href.substring(0, location.href.indexOf(location.pathname))
const SHOW_PLACEHOLDER     = '__showPlaceholder__'
const ROUTES = [
    {
        className: '__about__',
        pattern: new RegExp('^/?about.html$'),
        placeholderUrl: '/.about.html',
    },
    {
        className: '__blog__',
        pattern: new RegExp('^/?((writing/(\\d{4}/)?[^/]+\.html)|index.html|)$'),
        placeholderUrl: '/.blog.html',
    },
    {
        className: '__portfolio__',
        pattern: new RegExp('^/?portfolio/(index.html|)$'),
        placeholderUrl: '/.portfolio.html',
    }
]

const ENCOUNTERS = {}
const QUEUE_BEFORENEXT = []

let _currentPathname


export function beforeNext(fn) {
    QUEUE_BEFORENEXT.push(fn)
}


export async function init() {
    _currentPathname = location.pathname
    recordEncounter(location.href)

    // Schedule initial reveal
    showPlaceholder()
    setTimeout(hidePlaceholder, PLACEHOLDER_DURATION)

    // Wire up events
    document.addEventListener('DOMContentLoaded', async () => {

        await fetchPlaceholders()

        // Subscribe to events
        window.addEventListener('popstate', onPopState)
        enableHyperlinkInterception(document)
    })
}


//
// Internals
//


function didEncounter(url) {
    return ENCOUNTERS[url.split('/')[3]]
}


function disableHyperlinkInterception(context) {
    console.debug('[disableHyperlinkInterception]', context)
    Array.from(context.querySelectorAll('a'))
        .forEach(a => a.removeEventListener('click', onLinkClicked))
}


function enableHyperlinkInterception(context) {
    console.debug('[enableHyperlinkInterception]', context)
    Array.from(context.querySelectorAll('a'))
        .filter(a => getRoute(a.getAttribute('href') || a.getAttributeNS(NS_XLINK, 'href') || ''))
        .forEach(a => a.addEventListener('click', onLinkClicked))
}


async function fetchDom(href) {
    return new Promise((resolve, reject) => {
        const xhr = new XMLHttpRequest()
        xhr.responseType = 'document'
        xhr.open('GET', href)
        xhr.onreadystatechange = () => {
            if (xhr.readyState === 4) {
                if (xhr.status >= 400) {
                    onError()
                    throw new Error('fetchDom: failed to fetch DOM')
                }
                resolve(xhr.responseXML)
            }
        }
        xhr.send()
    })
}


async function fetchPlaceholders() {
    const t = Date.now()

    console.debug('[fetchPlaceholders] start')
    for (let route of ROUTES) {
        if (document.documentElement.classList.contains(route.className)) {
            continue
        }

        const dom = await fetchDom(route.placeholderUrl)
        const placeholder = dom.body.firstChild
        document.importNode(placeholder, true)
        document.querySelector('.placeholders').appendChild(placeholder)
    }

    console.debug('[fetchPlaceholders] finished in %dms', (Date.now() - t))
}


function flushBeforeNextQueue() {
    let func
    while ((func = QUEUE_BEFORENEXT.pop())) {
        try {
            func()
        }
        catch (e) {
            console.error('[flushBeforeNextQueue] callback queue item error:', func, e)
        }
    }
}


function getRoute(url) {
    return ROUTES.filter(r => r.pattern.test(url)).pop()
}


function hidePlaceholder() {
    document.documentElement.classList.remove(SHOW_PLACEHOLDER)
}


async function importScripts(context) {
    console.debug('[importScripts] import from context:', context)

    for (let script of Array.from(context.querySelectorAll('script'))) {
        script.parentNode.removeChild(script)

        if (!script.src.startsWith(SAME_ORIGIN) || script.type !== 'module') {
            continue
        }

        console.debug('[importScripts] found module:', script)

        const module = await import(script.src)
        try {
            await module.init()
        }
        catch (e) {
            console.error('[importScripts] script import failed: %s', script.src, e)
        }
    }
}


function onError() {
    disableHyperlinkInterception(document)
}


function onLinkClicked(event) {
    // Don't click-jack attempts to open new tabs
    if (event.metaKey || event.ctrlKey) {
        return
    }

    const url = this.getAttribute('href') || this.getAttributeNS(NS_XLINK, 'href')
    transitionTo(url)
    history.pushState(null, null, url)

    // Record pathname
    _currentPathname = location.pathname

    // Reset scroll position
    window.scrollTo(0, 0)

    // Only if we're sure nothing broke thus far
    event.preventDefault()
}


function onPopState() {
    if (location.pathname === _currentPathname) {
        console.debug('[onPopState] discarding hashchange')
        return  // Discard hashchange eventing from portfolio navigation
    }

    _currentPathname = location.pathname

    transitionTo(location.pathname + location.search)
}


function recordEncounter(url) {
    ENCOUNTERS[url.split('/')[3]] = true
}


function showPlaceholder() {
    document.documentElement.classList.add(SHOW_PLACEHOLDER)
}


async function transitionTo(url) {
    console.debug('[transitionTo] look up route `%s`', url)
    const route = getRoute(url)
    if (!route) {
        console.warn('[transitionTo] no route to `%s`', url)
        return
    }

    console.debug('[transitionTo] ===> `%s`', url)

    flushBeforeNextQueue()

    document.documentElement.className = route.className
    showPlaceholder()

    let endTime = Date.now()
    if (!didEncounter(url)) {
        recordEncounter(url)
        endTime += PLACEHOLDER_DURATION
    }

    const dom = await fetchDom(url)
    const nextMain = dom.querySelector('main')
    const previousMain = document.querySelector('main')

    enableHyperlinkInterception(nextMain)
    disableHyperlinkInterception(previousMain)

    // Swap the titles
    document.title = dom.title

    // Swap the dynamic stuff
    importScripts(nextMain)

    // Swap out the content
    document.body.insertBefore(nextMain, previousMain)
    document.body.removeChild(previousMain)

    // Swap out the footers
    const nextFooter = dom.querySelector('footer')
    const previousFooter = document.querySelector('footer')
    document.body.insertBefore(nextFooter, previousFooter)
    document.body.removeChild(previousFooter)

    // Schedule revelation
    setTimeout(hidePlaceholder, endTime - Date.now())
}
