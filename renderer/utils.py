import logging
import os
import threading
import time


TICK_MS = 700


class Watcher:
    def __init__(self):
        self._callbacks = {}
        self._paths     = {}  # type: dict[str, int]
        self._lock      = threading.RLock()
        self._log       = logging.getLogger('watcher')
        self._thread    = threading.Thread(name='watcher', target=self._run, daemon=True)

    def append(self, path):
        if path in self._paths:
            return

        path = os.path.abspath(path)

        mtime = -1
        if os.path.exists(path):
            mtime = os.stat(path).st_mtime_ns

        with self._lock:
            self._paths[path] = mtime

    def start(self):
        self._thread.start()

    def subscribe(self, callback, *, priority=1):
        """
        :type callback: function
        :type priority: int
        :rtype: function
        """

        def unsubscribe():
            with self._lock:
                if callback in self._callbacks:
                    self._callbacks.pop(callback)

        if not callable(callback):
            raise ValueError(f'callback {callback!r} is not callable')

        with self._lock:
            self._callbacks[callback] = int(priority)

        return unsubscribe

    def update(self, paths):
        for path in paths:
            self.append(path)

    def _run(self):
        interval = TICK_MS / 1000

        self._log.debug('watcher started: interval=%.1fs paths=%s', interval, len(self._paths))
        for path in sorted(self._paths):
            self._log.log(9, 'watch path: \033[2m%s\033[0m', path)

        # HACK -- crude but effective: it's a batteries-included solution and
        #         this is a dev server after all. ¯\_(ツ)_/¯
        while True:
            changed_files = set()

            t_start = time.time()

            with self._lock:
                paths = self._paths.copy()

            self._log.log(8, 'tick start')

            for path, previous_mtime in paths.items():
                current_mtime = previous_mtime
                if os.path.exists(path):
                    current_mtime = os.stat(path).st_mtime_ns

                if current_mtime != previous_mtime:
                    self._log.debug('changed: path=%s previous_mtime=%s current_mtime=%s', path, previous_mtime, current_mtime)
                    self._paths[path] = current_mtime

                    changed_files.add(path)

            if changed_files:
                changed_files = sorted(changed_files)
                with self._lock:
                    callbacks = self._callbacks.copy()

                self._log.debug('notify: callbacks=%d changes=%d', len(callbacks), len(changed_files))
                for callback, priority in sorted(callbacks.items(), key=lambda t: t[1], reverse=True):
                    try:
                        self._log.debug('notify: changes=%d pri=%d callback=%r', len(changed_files), priority, callback)
                        callback(changed_files)
                    except Exception as e:
                        self._log.error('subscriber error: %s', e, exc_info=e)

            self._log.log(8, 'tick complete in %.1fs', time.time() - t_start)

            time.sleep(interval)
