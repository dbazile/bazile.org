import argparse
import datetime
import logging
import os

from jinja2 import Environment, FileSystemLoader

from renderer import blog, server, static, portfolio, utils


WEB_ROOT     = 'public'
MARKDOWN_DIR = 'data/markdown'
XML_DIR      = 'data/xml'
TEMPLATE_DIR = 'renderer/templates'


# Collect script params
ap = argparse.ArgumentParser()
ap.add_argument('-v', '--verbose', action='count', default=0)
ap.add_argument('--clean', action='store_true')
ap.add_argument('--clean-only', action='store_true')
ap.add_argument('--server', action='store_true')
ap.add_argument('--host', default='127.0.0.1')
ap.add_argument('--port', default=8000, type=int)
params = ap.parse_args()


# Enter workspace
os.chdir(os.path.dirname(os.path.dirname(__file__)))


# Configure logging
logging.basicConfig(
    datefmt='%H:%M:%S',
    format='%(asctime)s [%(name)s] [%(funcName)-20s] %(levelname)-7s : %(message)s' if params.verbose else '[%(name)s] %(levelname)s : %(message)s',
    level=logging.DEBUG if params.verbose > 1 else logging.WARN,
)
logging.getLogger('renderer').setLevel((11 - params.verbose) if params.verbose else 20)


# Prepare Jinja2
env = Environment(
    loader=FileSystemLoader(TEMPLATE_DIR),
    lstrip_blocks=True,
    trim_blocks=True,
)
env.filters['format_datetime'] = datetime.datetime.strftime
env.globals['CURRENT_YEAR'] = datetime.datetime.now().year


# Clean
if params.clean or params.clean_only:
    blog.clean(WEB_ROOT)
    portfolio.clean(WEB_ROOT)
    static.clean(WEB_ROOT)

    if params.clean_only:
        exit()


# Render
blog.render(env, MARKDOWN_DIR, WEB_ROOT)
portfolio.render(env, XML_DIR, WEB_ROOT)
static.render(env, WEB_ROOT)


# Server
if params.server:
    watcher = utils.Watcher()
    blog.watch(watcher, env, MARKDOWN_DIR, WEB_ROOT)
    static.watch(watcher, env, WEB_ROOT)
    watcher.start()

    server.serve(watcher, params.host, params.port, WEB_ROOT)
