import http.server
import logging
import os
import socket
import time


REPO_ROOT = os.path.abspath(os.path.dirname(os.path.dirname(__file__)))

LOG = logging.getLogger(__name__)


def serve(watcher, host, port, web_root):
    """
    :type watcher: renderer.utils.Watcher
    :type host: str
    :type port: int
    :type web_root: str
    """

    class RequestHandler(http.server.SimpleHTTPRequestHandler):
        protocol_version = 'HTTP/1.1'

        def __init__(self, request, client_addr, server):
            """
            :type request: socket.socket
            :type client_addr: (str, int)
            :type server: Server
            """

            LOG.debug('[%s:%s] created RequestHandler', *client_addr)

            try:
                super().__init__(request, client_addr, server, directory=os.path.join(REPO_ROOT, web_root))
            except (BrokenPipeError, ConnectionError) as e:
                LOG.debug('%s: %s:%d', e, *self.client_address)

        def do_GET(self):
            if self.headers.get('accept') == 'text/event-stream':
                self.handle_sse_request()
                return

            try:
                super().do_GET()
            except (BrokenPipeError, ConnectionError) as e:
                LOG.debug('%s: %s:%d', e, *self.client_address)

        def handle_sse_request(self):
            def callback(_):
                LOG.debug('[sse:%s:%d] notify client', *self.client_address)
                cs.send(b'data: RELOAD\n\n')

            cs = self.request  # type: socket.socket

            unsubscribe = watcher.subscribe(callback, priority=-1)  # ensure it runs after all other subscribers
            try:
                LOG.debug('[sse:%s:%d] client connected', *self.client_address)

                cs.send('\r\n'.join([
                    f'HTTP/1.1 200 OK',
                    f'server: {self.version_string()}',
                    f'cache-control: no-cache',
                    f'content-type: text/event-stream',
                    f'',
                    f'',
                ]).encode())

                cs.send(b'data: HELLO\n\n')
                while cs.recv(1):  # HACK -- maybe not legit but proactively detects disconnect
                    time.sleep(1)
                cs.shutdown(socket.SHUT_RDWR)
            except BrokenPipeError:
                LOG.debug('[\033[2m%s:%d\033[0m] client disconnected', *self.client_address)
            finally:
                LOG.debug('[sse:%s:%d] client shutdown', *self.client_address)
                cs.close()
                unsubscribe()

        def log_error(self, msg, *args):
            LOG.error(msg, *args)

        def log_request(self, code):
            color = '36'

            if 200 <= code <= 299:
                color = 32
            if 300 <= code <= 399:
                color = 36
            elif 400 <= code <= 599:
                color = 31

            LOG.info('\033[%sm%s\033[0m %s %s', color, code.value, self.command, self.path)

        # HACK -- prevent iOS from caching CSS during dev previews
        def send_response(self, code, message=None):
            super().send_response(code, message=message)
            self.send_header('cache-control', 'no-cache')

        def send_error(self, code, message=None, explain=None):
            path_404 = os.path.join(web_root, '404.html')

            if code == http.server.HTTPStatus.NOT_FOUND and os.path.exists(path_404):
                self.send_response(code, message)
                self.send_header('connection', 'close')
                self.end_headers()

                with open(path_404, 'rb') as f:
                    self.wfile.write(f.read())
            else:
                super().send_error(code, message=message, explain=explain)

    server = http.server.ThreadingHTTPServer((host, port), RequestHandler)

    LOG.info('listen: \033[36mhttp://%s:%s\033[0m', host, port)
    try:
        server.serve_forever()
    except KeyboardInterrupt:
        pass
