import glob
import logging
import os
import threading
import time


TEMPLATES = {
    '404.jinja2':                '404.html',
    'about.jinja2':              'about.html',
    'about__loading.jinja2':     '.about.html',
    'blog__loading.jinja2':      '.blog.html',
    'portfolio__loading.jinja2': '.portfolio.html',
    'search.jinja2':             'search.html',
}

LOG = logging.getLogger(__name__)


def clean(output_dir):
    """
    :type output_dir: str
    """

    for output_file in _list_pages(output_dir).values():
        if not os.path.exists(output_file):
            continue

        LOG.info('clean %s', output_file)
        os.unlink(output_file)


def render(env, output_dir):
    """
    :type env:        jinja2.Environment
    :type output_dir: str
    """

    for template_file, output_file in _list_pages(output_dir).items():
        _render(env, template_file, output_file)


def watch(watcher, env, output_dir):
    """
    :type watcher:    renderer.utils.Watcher
    :type env:        jinja2.Environment
    :type output_dir: str
    :rtype: threading.Thread
    """

    GLOB_CSS      = os.path.join(output_dir, 'styles/*.css')
    GLOB_JS       = os.path.join(output_dir, 'lib/*.js')
    MAIN_TEMPLATE = os.path.abspath(env.get_template('_layout.jinja2').filename)

    ALL_ASSET_PATHS    = set()
    ALL_TEMPLATE_PATHS = set(os.path.abspath(env.get_template(s).filename) for s in _list_pages(output_dir))

    def callback(raw):
        pages = _list_pages(output_dir)

        template_paths = ALL_TEMPLATE_PATHS.intersection(raw)

        if MAIN_TEMPLATE in raw:
            LOG.info('template changed: %s', MAIN_TEMPLATE)
            template_paths.update(pages.keys())  # Schedule rerender for all pages

        for filepath in template_paths:
            template_file = os.path.basename(filepath)
            LOG.debug('template changed: %s', os.path.relpath(filepath))
            _render(env, template_file, pages[template_file])

        for filepath in ALL_ASSET_PATHS.intersection(raw):
            LOG.info('asset changed: %s', os.path.relpath(filepath))

    def update_paths():
        ALL_ASSET_PATHS.clear()
        ALL_ASSET_PATHS.update(os.path.abspath(s) for s in glob.glob(GLOB_JS, recursive=True))
        ALL_ASSET_PATHS.update(os.path.abspath(s) for s in glob.glob(GLOB_CSS, recursive=True))

        watcher.update(ALL_TEMPLATE_PATHS.union(ALL_ASSET_PATHS))

    def worker():
        LOG.info('watch: %d templates, %s, %s', len(ALL_TEMPLATE_PATHS), GLOB_JS, GLOB_CSS)

        watcher.subscribe(callback)

        # Continually watch for new files
        while True:
            update_paths()
            time.sleep(1)

    update_paths()

    t = threading.Thread(target=worker, daemon=True)
    t.start()

    return t


###############################################################################

#
# Internals
#

def _list_pages(output_dir):
    """
    :type output_dir: str
    :rtype: dict[str, str]
    """
    return {k: os.path.join(output_dir, v) for k, v in sorted(TEMPLATES.items())}


def _render(env, src_path, dest_path):
    try:
        template = env.get_template(src_path)

        with open(dest_path, 'w') as f:
            f.write(template.render())
            LOG.info('wrote {}'.format(dest_path))
    except Exception as e:
        LOG.fatal('render error: %e: src=%s dest=%s', e, src_path, dest_path)
        raise e
