import glob
import logging
import os
import re
import threading
import time

from markdown import Markdown
from markdown.inlinepatterns import SimpleTagInlineProcessor
from markdown.preprocessors import Preprocessor

import yaml


DEFAULT_TYPE     = 'text'
TYPE_IMAGE       = 'image'
TYPE_LINK        = 'link'
TYPE_QUOTE       = 'quote'
TYPE_TEXT        = 'text'

PATH_QUALIFIED   = 'writing/{date:%Y}/{id}.html'
PATH_UNQUALIFIED = 'writing/{id}.html'

PATTERN_EXTERNAL = re.compile(r'^(https?:)//')
PATTERN_MARKDOWN = re.compile(r'^---\n(?P<meta>.*)\n---\n\n?(?P<body>.*)$', re.MULTILINE | re.DOTALL)

LOG = logging.getLogger(__name__)


def clean(output_dir):
    """
    :type output_dir: str
    """

    filepaths = sorted(glob.glob(os.path.join(output_dir, 'writing/*.html')))
    filepaths.extend(sorted(glob.glob(os.path.join(output_dir, 'writing/2???/*.html'))))
    filepaths.extend(sorted(glob.glob(os.path.join(output_dir, 'writing/2???/'))))
    filepaths.extend(sorted(glob.glob(os.path.join(output_dir, 'index.html'))))

    for filepath in filepaths:
        LOG.info('clean %s', filepath)
        if os.path.isdir(filepath):
            os.rmdir(filepath)
        else:
            os.unlink(filepath)


def render(env, markdown_dir, output_dir):
    """
    :type env:          jinja2.Environment
    :type markdown_dir: str
    :type output_dir:   str
    """

    posts = []
    failures = []

    pubdir = '/' + os.path.basename(output_dir) + '/'

    for filepath in _globmds(markdown_dir):
        try:
            post = _deserialize_post(filepath, pubdir)
            _render_post(env, post, output_dir)
            posts.append(post)
        except ValidationError as err:
            failures.append(filepath)
            LOG.error('Failed on %s: %s', filepath, err)
        except Exception as err:
            LOG.fatal('Could not process `%s`\n\t%s: %s',
                      filepath, err.__class__.__name__, err)
            raise

    posts = _sort_posts(posts)
    _render_index(env, posts, output_dir)

    # Report card
    if failures:
        LOG.warning('*** %s failures: %s', len(failures), ', '.join(failures))


def watch(watcher, env, markdown_dir, output_dir):
    """
    :type watcher:      renderer.utils.Watcher
    :type env:          jinja2.Environment
    :type markdown_dir: str
    :type output_dir:   str
    :rtype: threading.Thread
    """

    PUBDIR = f'/{os.path.basename(output_dir)}/'

    ALL_MARKDOWN_PATHS = set()
    ALL_TEMPLATE_PATHS = {
        os.path.abspath('renderer/templates/blog.jinja2'),
        os.path.abspath('renderer/templates/blogpost.jinja2'),
        os.path.abspath('renderer/templates/_layout.jinja2'),
    }

    def callback(raw):
        markdown_paths = ALL_MARKDOWN_PATHS.intersection(raw)
        template_paths = ALL_TEMPLATE_PATHS.intersection(raw)

        for filepath in template_paths:
            LOG.info('template changed: %s', os.path.relpath(filepath))
            markdown_paths.update(ALL_MARKDOWN_PATHS)  # Schedule rerender for all posts
            break

        rerender_index = False
        for filepath in markdown_paths:
            rerender_index = True
            try:
                post = _deserialize_post(filepath, PUBDIR)
            except Exception as e:
                LOG.error('could not deserialize %r: %s', filepath, e)
                continue

            try:
                _render_post(env, post, output_dir)
            except Exception as e:
                LOG.error('could not render %r: %s', filepath, e)
                continue

        if rerender_index:
            try:
                posts = [_deserialize_post(s, PUBDIR) for s in _globmds(markdown_dir)]
                _render_index(env, _sort_posts(posts), output_dir)
            except Exception as e:
                LOG.error('could not render index %r: %s', filepath, e)

    def update_paths():
        ALL_MARKDOWN_PATHS.clear()
        ALL_MARKDOWN_PATHS.update(os.path.abspath(s) for s in _globmds(markdown_dir))

        watcher.update(ALL_TEMPLATE_PATHS.union(ALL_MARKDOWN_PATHS))

    def worker():
        LOG.info('watch: %s/*.md', markdown_dir)

        watcher.subscribe(callback)

        # Continually watch for new files
        while True:
            update_paths()
            time.sleep(1)

    update_paths()

    t = threading.Thread(target=worker, daemon=True)
    t.start()

    return t


###############################################################################

#
# Internals
#

def _deserialize_post(filepath, pubdir):
    LOG.debug('READ %s', filepath)

    with open(filepath) as fp:
        matches = PATTERN_MARKDOWN.search(fp.read())

    if not matches:
        raise ValidationError('YAML header is missing')

    raw_meta, raw_body = matches.groups()

    m = Markdown(extensions=[
        'markdown.extensions.smarty',
        'markdown.extensions.fenced_code',
        'markdown.extensions.tables',
        'markdown.extensions.toc',
        'markdown.extensions.sane_lists',
    ])

    m.preprocessors.register(LocalPathResolver(pubdir), 'local-path-resolver', 0)
    m.inlinePatterns.register(SimpleTagInlineProcessor('(~~([^~]+)~~)', 'del'), 'strikeout', 0)

    post = {
        'id': _generate_id(filepath),
        'type': DEFAULT_TYPE,
        'body': m.convert(raw_body),
        'aliases': [],
    }

    meta = yaml.safe_load(raw_meta)
    if not meta:
        raise ValidationError('YAML header is malformed')

    for key, value in meta.items():
        key = key.lower()
        if key not in ('abstract', 'crop', 'date', 'subject', 'tags', 'type', 'url', 'aliases'):
            LOG.warning('\033[33m%s: unknown meta property %r = %r\033[0m', filepath, key, value)
            continue

        if key == 'abstract':
            value = m.convert(value)

        elif key == 'crop':
            value = bool(value)

        elif key == 'aliases':
            if not isinstance(value, list):
                LOG.warning('\033[33m%s: meta property %r must be a list, got %s\033[0m', filepath, key, type(value))
                continue

            value = list(_generate_id(s) for s in sorted(set(value).difference([post['id']])))

        post[key] = value

    _validate(post)

    # Special Case: Quotes and images need abstract to mirror body
    if post['type'] in ('quote', 'image'):
        post['abstract'] = post['body']

    # Special Case: Image URLs may point to a repo-relative path
    if post['type'] == 'image':
        post['url'] = _resolve_local(post['url'], pubdir)

    return post


def _generate_id(filepath):
    basename, _ = os.path.splitext(os.path.basename(filepath))
    return re.sub(r'\W+', '_', basename).strip('_ ').lower()


def _globmds(markdown_dir):
    return sorted(glob.glob(os.path.join(markdown_dir, '*.md')))


def _render_post(env, post: dict, output_dir):
    template = env.get_template('blogpost.jinja2')

    context = post.copy()

    # Type-specific Logic
    if context['type'] == TYPE_IMAGE:
        context['body'] = '<img src="{0}"/>'.format(context['url'])
    elif context['type'] == TYPE_QUOTE:
        context['abstract'] = ''
    elif context['type'] == TYPE_LINK:
        context['body'] = '<a href="{0}">{0}</a>'.format(context['url'])

    for alias in [post['id']] + post['aliases']:
        filepath = os.path.join(output_dir, PATH_QUALIFIED.format(date=post['date'], id=alias))

        # Ensure parent dir
        parent_dir = os.path.dirname(filepath)
        if not os.path.exists(parent_dir):
            LOG.debug('Creating parent dir `%s`', parent_dir)
            os.makedirs(parent_dir)

        LOG.debug('Before write `%s`', filepath)
        with open(filepath, 'w') as fp:
            fp.write(template.render(**context))

        # Was calling this a hack but I suppose it's just scrub-tier SEO ¯\_(ツ)_/¯
        unq_filepath = os.path.join(output_dir, PATH_UNQUALIFIED.format(date=post['date'], id=alias))
        LOG.debug('Copy to unqualified location `%s`', unq_filepath)
        with open(unq_filepath, 'w') as fp:
            fp.write(template.render(**context).replace(
                '<head>',
                '<head>\n\n  <meta http-equiv="refresh" content="0; url=/{}" />\n'.format(PATH_QUALIFIED.format(**post)),
            ))

        LOG.info('wrote %s', filepath)


def _render_index(env, posts, output_dir):
    template = env.get_template('blog.jinja2')

    contexts = []
    for post in posts:
        context = post.copy()
        if context['type'] in (TYPE_TEXT, TYPE_QUOTE):
            context['url'] = PATH_QUALIFIED.format(**post)
        contexts.append(context)

    filepath = os.path.join(output_dir, 'index.html')
    with open(filepath, 'w') as fp:
        LOG.debug('Before write `%s`', filepath)
        fp.write(template.render(posts=contexts))
    LOG.info('wrote %s', filepath)


def _resolve_local(raw_path: str, pubdir: str):
    if PATTERN_EXTERNAL.match(raw_path):
        LOG.debug('Skip non-local: %s', raw_path)
        return raw_path

    # Rewrite public path
    resolved_path = re.sub('^' + re.escape(pubdir), '/', raw_path)

    # Discard rewrite if file can't be resolved
    abs_path = os.curdir + raw_path
    if not os.path.exists(abs_path):
        LOG.warn('Found reference to nonexistent local: %s', abs_path)
        return raw_path

    LOG.log(9, 'resolve local: \033[2m%s\033[0m -> \033[2;34m%s\033[0m', raw_path, resolved_path)

    return resolved_path


def _sort_posts(posts):
    return sorted(posts, reverse=True, key=lambda post: post['date'])


def _validate(post):
    # Required fields
    for key in ('id', 'type', 'tags', 'subject'):
        if key not in post:
            raise ValidationError('missing `{}`'.format(key), post)

    # Images and Links must point to something
    if post.get('type') in ('image', 'link'):
        if 'url' not in post:
            raise ValidationError('missing `{}`'.format('url'), post)

    # Only images have crop setting
    if post.get('type') != 'image' and 'crop' in post:
        raise ValidationError('only image posts can have \'crop\' parameter')


#
# Markdown Processors
#


class LocalPathResolver(Preprocessor):
    def __init__(self, pubdir, **kwargs):
        super().__init__(**kwargs)
        self.pattern = re.compile(r'((?:"{0}[^"]+")|(?:\({0}[^)]+\)))'.format(re.escape(pubdir)))
        self.pubdir = pubdir

    def run(self, lines: list):
        new_lines = []

        for i, line in enumerate(lines, 1):
            LOG.log(9, 'LocalPathResolver: inspect %3d: \033[2m%s\033[0m', i, line)

            for m in self.pattern.finditer(line):
                local = m.group()[1:-1]

                LOG.log(9, 'LocalPathResolver: resolving local path: %s', local)

                new_local = _resolve_local(local, self.pubdir)

                # Apply the change
                line = re.sub(re.escape(local), new_local, line, 1)

                LOG.log(9, 'LocalPathResolver: changed line %s: %s', i, line)

            new_lines.append(line)

        return new_lines


#
# Errors
#

class ValidationError(Exception):
    def __init__(self, message, post=None):
        self.message = message
        self.post = post

    def __str__(self):
        return '{}: {}'.format(self.__class__.__name__, self.message)
